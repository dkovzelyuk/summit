import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { SectionBlockComponent } from './section-block/section-block.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '',   redirectTo: '/build', pathMatch: 'full' },
  { path: 'build', component: SectionBlockComponent },
  { path: 'plan', component: SectionBlockComponent },
  { path: 'innovate', component: SectionBlockComponent },
  { path: 'interact', component: SectionBlockComponent },
  { path: 'scale', component: SectionBlockComponent },
  { path: 'service', component: SectionBlockComponent },
  { path: 'enhance', component: SectionBlockComponent },
  { path: 'general', component: SectionBlockComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    SectionBlockComponent,
    WrapperComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {useHash: false}),
    ScrollToModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
