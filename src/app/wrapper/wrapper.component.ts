import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.css']
})
export class WrapperComponent implements OnInit {

  public pageTitle: string = "Video Archive";

  constructor(private router: Router) { }

  navigate($event) {
    this.router.navigate([$event.nextId]);
  }

  ngOnInit()  {}

}
