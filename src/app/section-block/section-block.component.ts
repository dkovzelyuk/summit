import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../service/items.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-section-block',
  templateUrl: './section-block.component.html',
  styleUrls: ['./section-block.component.css']
})
export class SectionBlockComponent implements OnInit {

  public items : any[];
  public sessionTitle : string;
  public showId: number;
  public contentToDisplay: {};

  constructor( private service: ItemsService, private route: ActivatedRoute) { }

  showDetails(Id) {
    this.showId = Id;

    this.contentToDisplay = this.items.filter(
        item => item.Id === Id
      )[0];
  }

  getBlockData() {
    this.service.getData().subscribe(response => {
        this.items = response['Items'].filter( 
            item => item.Track.Title.toLowerCase() === this.route.snapshot.url[0].path 
          );

        this.sessionTitle = this.items[0].Track.Title;
        this.showId = this.items[0].Id;
        this.contentToDisplay = this.items[0];
    });
  }

  ngOnInit() {
    this.sessionTitle = '';
    this.getBlockData();
  }

}
